const imagePath = './public/img';

export class Console {

    constructor(consoleElement) { this._consoleElement = consoleElement; }

    static _createLiElement() { return document.createElement('li'); }

    static addKeyPressEventListener(eventListener) { document.addEventListener('keypress', eventListener); }

    print(text) {
        const liElement = Console._createLiElement();
        liElement.innerText = String(text);
        this._consoleElement.appendChild(liElement);
    }

    printImage(image, alt = '') {
        const liElement = Console._createLiElement();
        const imgElement = document.createElement('img');
        imgElement.setAttribute('src', `${imagePath}/${image}`);
        imgElement.setAttribute('alt', alt);
        liElement.appendChild(imgElement);
        this._consoleElement.appendChild(liElement);
    }
}
