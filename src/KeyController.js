import {Console} from "./Console";
import {KeyPressedEvent} from "./events/KeyPressedEvent";

export class KeyController {

    constructor(player) {
        this._player = player;

        Console.addKeyPressEventListener(e =>
            this._player.currentRoom.getEvents(KeyPressedEvent)
                .filter(event => event.shouldExecute(e))
                .forEach(event => event.execute())
        );
    }
}
