export class Player {

    constructor(startRoom) { this._currentRoom = startRoom; }

    get currentRoom() { return this._currentRoom; }
}
