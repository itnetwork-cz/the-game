import {AbstractEvent} from "./AbstractEvent";

export class KeyPressedEvent extends AbstractEvent {

    constructor(key) {
        super();
        this._key = String(key).toLowerCase();
    }

    shouldExecute(event) { return event.key === this._key; }
}
