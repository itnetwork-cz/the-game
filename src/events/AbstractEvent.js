export class AbstractEvent {

    constructor() { this._actions = []; }

    addAction(action) {
        this._actions.push(action);
        return this;
    }

    execute() { for (const action of this._actions) action.execute(); }
}
