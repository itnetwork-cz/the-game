import {AbstractAction} from "./AbstractAction";

export class PrintAction extends AbstractAction {

    constructor(console, text) {
        super();
        this._console = console;
        this._text = text;
    }

    execute() { this._console.print(this._text); }
}
