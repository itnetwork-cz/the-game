import {Console} from "./Console";
import {Room} from "./Room";
import {Player} from "./Player";
import {KeyPressedEvent} from "./events/KeyPressedEvent";
import {PrintAction} from "./actions/PrintAction";
import {KeyController} from "./KeyController";

export class Game {

    constructor(consoleElement) { this._console = new Console(consoleElement); }

    run() {
        // TODO: Implement loading of a game.
        return this;
    }
}
