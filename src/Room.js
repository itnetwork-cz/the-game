export class Room {

    constructor() { this._events = []; }

    addEvent(event) {
        this._events.push(event);
        return this;
    }

    getEvents(eventClass = null) {
        return !eventClass ? this._events() : this._events.filter(event => event instanceof eventClass);
    }
}
